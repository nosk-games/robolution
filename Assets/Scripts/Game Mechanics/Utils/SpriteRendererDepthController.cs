﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SpriteRendererDepthController : MonoBehaviour
{

    public void LateUpdate()
    {
        SpriteRenderer[] srs = GameObject.FindObjectsOfType<SpriteRenderer>();
        foreach (SpriteRenderer sr in srs) sr.sortingOrder = Mathf.CeilToInt(calculateBase(sr.gameObject).y) * -1;

        GameObject[] tagObjects = GameObject.FindGameObjectsWithTag("InnerDepth");
        foreach (GameObject tagObject in tagObjects)
        {
            tagObject.GetComponent<SpriteRenderer>().sortingOrder = tagObject.transform.parent.GetComponent<SpriteRenderer>().sortingOrder +1;
        }



    }

    private Vector3 calculateBase(GameObject entity)
    {
        Vector3 ext = entity.GetComponent<SpriteRenderer>().bounds.extents;
        Vector3 pos = entity.GetComponent<Transform>().position;

        return new Vector3(pos.x, pos.y - ext.y, pos.z);
    }
}
