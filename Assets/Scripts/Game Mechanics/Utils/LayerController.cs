﻿using UnityEngine;
using System.Collections;

public class LayerController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    //void OnTriggerStay2D(Collider2D other)
    //{
    //    SpriteRenderer sr = other.gameObject.GetComponent<SpriteRenderer>();
    //    sr.sortingOrder = this.GetComponent<SpriteRenderer>().sortingOrder - 1;
    //}

    //public void OnTriggerStay2D(Collider2D other)
   void OnTriggerEnter2D(Collider2D other)
    {
        // Is they use the same layer, it may be a conflict
        if (this.GetComponentInParent<SpriteRenderer>().sortingLayerID == other.GetComponentInParent<SpriteRenderer>().sortingLayerID)
        {
            Debug.Log("entra");
            Vector3 selfBase = calculateBase(this);
            Vector3 otherBase = calculateBase(other);
            SpriteRenderer otherRenderer =  other.GetComponentInParent<SpriteRenderer>();
            SpriteRenderer selfRenderer =  this.GetComponentInParent<SpriteRenderer>();


            //Debug.Log(this.name + " " + selfBase.ToString() + " | " + other.name + " " + otherBase.ToString());
            //Debug.Log(this.name + " " + selfRenderer.sortingOrder.ToString() + " | " + other.name + " " + otherRenderer.sortingOrder.ToString());
            if (selfBase.y < otherBase.y) // self is lower so must be in front ( SEKF IN FRONT; OTHER BACK )
            {
                //Debug.Log(this.name + " davant");
                while (otherRenderer.sortingOrder >= selfRenderer.sortingOrder) otherRenderer.sortingOrder--;
                //otherRenderer.sortingOrder--;
            }
            else
            {
                //Debug.Log(this.name + " darrere");
                while (otherRenderer.sortingOrder <= selfRenderer.sortingOrder) otherRenderer.sortingOrder++;
                //otherRenderer.sortingOrder++;
            }
        }


        

    }

    private Vector3 calculateBase(Component entity)
    {
        Vector3 ext = entity.GetComponentInParent<SpriteRenderer>().bounds.extents;
        Vector3 pos = entity.GetComponentInParent<Transform>().position;

        return new Vector3(pos.x, pos.y - ext.y, pos.z);
    }

    //void OnTriggerExit2D(Collider2D other)
    //{
    //    Debug.Log("Ha sortit");
    //    other.gameObject.GetComponent<SpriteRenderer>().sortingOrder = this.GetComponent<SpriteRenderer>().sortingOrder + 1;
    //}


    // Update is called once per frame
    void Update()
    {

    }


}
