﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public string scene;
    public string point;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag.Equals("Player")) // if a player entered in the teleport zone
        {
            DontDestroyOnLoad(this.gameObject); // Change scene cause to destroy all objects and then coroutines are stopped. Must save this object
            AsyncOperation op = SceneManager.LoadSceneAsync(scene);
            op.allowSceneActivation = true; // Active scene as soon as possible
            StartCoroutine("setPlayerPosition", op); 
        }
    }

    // This coroutine waits until the scene is loaded and then set the player's and camera position
    private IEnumerator setPlayerPosition(AsyncOperation op)
    {  
        // Wait until loaded
        while (!op.isDone)
        {
            yield return null;
        }

        Vector3 pointPos = GameObject.Find(point).transform.position; // Find spawn point on new scene
        Transform player = GameObject.FindGameObjectWithTag("Player").transform; // Find player
        player.position = pointPos; // move player
        CameraController c = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>(); // Find camera
        if (c.enabled) // if camera must follow the player, set position and target
        {
            c.transform.position = new Vector3(pointPos.x, pointPos.y, c.transform.position.z);
            c.target = player;
        }
        Destroy(this.gameObject); // destroy this gameobject because is not necessary anymore
        yield return null;
    }

}
