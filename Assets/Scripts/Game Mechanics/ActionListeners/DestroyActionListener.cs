﻿using UnityEngine;
using System.Collections;

public class DestroyActionListener : MonoBehaviour, ActionListener  {


    public void onAction(GameObject actioner)
    {
        GameObject.Destroy(this.gameObject);
    }
}
