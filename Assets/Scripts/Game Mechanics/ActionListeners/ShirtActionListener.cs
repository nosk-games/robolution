﻿using UnityEngine;
using System.Collections;

public class ShirtActionListener : MonoBehaviour, ActionListener  {

    public GameObject canvas;

    public void onAction(GameObject actioner)
    {
        canvas.active = true;
        Animator atr = actioner.GetComponent<Animator>();
        atr.Play("agarrando");
        actioner.GetComponent<PlayerMovementController>().canMove = false;

        StartCoroutine(waitAnimation(atr,actioner));

    }

    private IEnumerator waitAnimation(Animator atr, GameObject actioner)
    {
        yield return null;

        while (atr.GetCurrentAnimatorClipInfo(0)[0].clip.name.Equals("agarrando"))
        {
            yield return null;
        }

        actioner.GetComponent<PlayerMovementController>().canMove = true;
        canvas.active = false;
    }

}
