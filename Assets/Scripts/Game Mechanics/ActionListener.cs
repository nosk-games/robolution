﻿using UnityEngine;
using System.Collections;

public interface ActionListener {

    void onAction(GameObject actioner);
	
}
