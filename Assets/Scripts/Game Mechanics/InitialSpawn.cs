﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class InitialSpawn : MonoBehaviour
{

    public GameObject playerPrefab;

    // Use this for initialization
    void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player == null) player = GameObject.Instantiate(playerPrefab);
        player.transform.position = new Vector3(35f, 21f, 0f);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
