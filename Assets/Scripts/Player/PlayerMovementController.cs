﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class PlayerMovementController : MonoBehaviour
{
    public bool canMove = true;
    public float SPEED = 10f;
    private Animator animator;
    //private bool lastWasDiagonal;
    private int lastFaceing;
    private float lastVy;
    private float lastVx;
    private Vector3 lastPosition;
    private int faceing = -1;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        lastPosition = GetComponent<Rigidbody2D>().transform.position;
        //lastWasDiagonal = false;
    }

    void FixedUpdate()
    {
        if (!canMove) return;

        float vX = 0f;
        float vY = 0f;

        if (Input.GetKey(KeyCode.D)) // Right
        {
            vX = 1;
            faceing = (int)Faceing.East;
        }
        else if (Input.GetKey(KeyCode.A)) // Left
        {
            vX = -1;
            faceing = (int)Faceing.West;
        }

        if (Input.GetKey(KeyCode.W)) // Back
        {
            vY = 1;
            faceing = (int)Faceing.North;
        }
        else if (Input.GetKey(KeyCode.S)) // Front 
        {
            vY = -1;
            faceing = (int)Faceing.South;
        }

        GetComponent<Rigidbody2D>().velocity = new Vector2(vX, vY).normalized * SPEED;

        bool isAtSamePosition = (lastPosition - GetComponent<Rigidbody2D>().transform.position).sqrMagnitude < 0.001f;
        bool moving = (vX != 0 || vY != 0) && !isAtSamePosition;
        animator.SetBool("moving", moving);



        lastPosition = GetComponent<Rigidbody2D>().transform.position;

        // Faceing
        if (faceing >= 0)
        {
            bool movingDiagonal = (vY != 0 && vX != 0);
            if (!movingDiagonal || (movingDiagonal && (lastVx != vX && lastVy != vY)  )) // Is not moving in diagonal or is moving but not the same
            {
                lastFaceing = faceing;
                lastVy = vY;
                lastVx = vX;
            }
            else
            {
                faceing = lastFaceing;
            }

            animator.SetInteger("faceing", faceing);
        }

    }

    public Faceing getPlayerOrientation()
    {
        return (Faceing)faceing;
    }

    // Update is called once per frame
    //void Update()
    //{

    //}
}
