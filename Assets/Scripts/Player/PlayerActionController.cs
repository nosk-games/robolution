﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System.Linq;

public class PlayerActionController : MonoBehaviour {

    public float RAYCAST_DISTANCE = 15f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.F))
        {
            Faceing orientation = this.GetComponent<PlayerMovementController>().getPlayerOrientation();

            Transform t = this.GetComponent<Transform>();
            Vector2 origin = new Vector2(t.position.x,t.position.y - this.GetComponent<SpriteRenderer>().bounds.extents.y * 0.95f );
            Vector2 direction = new Vector2(0,0);
            switch (orientation)
            {
                case Faceing.North:
                    direction = Vector2.up;
                    break;

                case Faceing.South:
                    direction = Vector2.down;
                    break;

                case Faceing.West:
                    direction = Vector2.left;
                    break;

                case Faceing.East:
                    direction = Vector2.right;
                    break;
            }

            RaycastHit2D[] hits = Physics2D.RaycastAll(origin, direction, RAYCAST_DISTANCE);//, LayerMask.NameToLayer("Default"),-Mathf.Infinity,Mathf.Infinity);
            if ( hits.Length > 0)
            {
                RaycastHit2D hit = hits.FirstOrDefault(x => x.transform != null && x.transform.tag != "Player" && x.transform.gameObject.GetComponent<ActionListener>() != null);
                if (hit.transform != null)
                {
                    Debug.Log(hit.transform.name);
                    hit.transform.gameObject.GetComponent<ActionListener>().onAction(this.gameObject);
                }
            }

        }
	
	}
}
