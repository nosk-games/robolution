﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    //private Vector3 velocity = Vector3.zero;
    public Transform target;

    private readonly float pixelToUnits = 1f;

    void Awake()
    {
        this.GetComponent<Camera>().orthographicSize =  Screen.height / 4;
    }

    // Use this for initialization
    void Start()
    {
        GameObject p = GameObject.FindGameObjectWithTag("Player");
        if (p != null)
        {
            this.transform.position = new Vector3(p.transform.position.x, p.transform.position.y, this.transform.position.z);
            this.target = p.transform;

        }
    }

    void Update()
    {
        if (target != null)
        {
            float rounded_x = roundToNearestPixel(target.transform.position.x);
            float rounded_y = roundToNearestPixel(target.transform.position.y);

            Vector3 newPos = new Vector3(rounded_x, rounded_y, this.transform.position.z);
            this.transform.position = newPos;
        }
    }

    public float roundToNearestPixel(float unityUnits)
    {
        float valueInPixels = unityUnits * pixelToUnits;
        valueInPixels = Mathf.Round(valueInPixels);
        float roundedUnityUnits = valueInPixels * (1 / pixelToUnits);
        return roundedUnityUnits;
    }
}
